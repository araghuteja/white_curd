package com.enigma;

import com.enigma.controller.SimpleErrorSim;
import com.enigma.controller.SimpleExceptionSim;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));


        while(true){
            System.out.println("Simple Java Nightmare Simulator");
            System.out.println("-------------------------------");
            System.out.println("Press 1 for Out Of Memory Error simulation");
            System.out.println("Press 2 for Stack Overflow Error simulation");
            System.out.println("Press 3 for Arithmetic Exception simulation");
            System.out.println("Press 4 for Checked Exception simulation");
            System.out.println("Press 5 for Checked Exception simulation");
            System.out.println("Press z to Exit");

            String input = reader.readLine();

            while(input.isEmpty()) {
                input = reader.readLine();
            }

            if (input.equals("1")){
                SimpleErrorSim.OutOfMemoryError();
            } else if (input.equals("2")) {
                SimpleErrorSim.stackOverflowError(5);
            } else if (input.equals("3")) {
                SimpleExceptionSim.arithmeticException(5, 9);
            } else if (input.equals("4")) {
                SimpleExceptionSim.getNonExistingFileException();
            } else if (input.equals("5")) {
                SimpleExceptionSim simpleExceptionSim = new SimpleExceptionSim();
                simpleExceptionSim.addNewUser(5, "Hulk");
            } else if (input.equals("z"))
                break;
        }
        System.out.println("All Done. Bye!");
    }
}
