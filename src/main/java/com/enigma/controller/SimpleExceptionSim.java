package com.enigma.controller;

import com.enigma.exceptions.UserExistisException;
import com.enigma.service.SimpleService;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class SimpleExceptionSim {

    public static void arithmeticException(int i, int j) {
        int k = i / (j - j);
    }

    public static void getNonExistingFileException() throws IOException {
        FileReader file = new FileReader("a.txt");
    }

    public  SimpleService ss = new SimpleService();
    public void addNewUser(int id, String name) throws Exception {
        try{
            ss.addNewUser(id, name);
        } catch (UserExistisException e) {
            e.printStackTrace();
            throw new Exception("Please select a different ID as a user with that ID already exists in the system");
        }
    }

}
