package com.enigma.api;

public class ClassA {
    private Integer id;

    public ClassA() {

    }

    public ClassA(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
