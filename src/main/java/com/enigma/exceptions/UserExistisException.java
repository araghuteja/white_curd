package com.enigma.exceptions;

public class UserExistisException extends Exception {
    public UserExistisException(String s)
    {
        super(s);
    }
}